//
//  main.m
//  AppNet
//
//  Created by Joseph Southern on 10/3/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JSAppDelegate class]));
    }
}
