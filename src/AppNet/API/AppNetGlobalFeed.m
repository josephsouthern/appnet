//
//  AppNetGlobalFeed.m
//  AppNet
//
//  Created by Joseph Southern on 10/5/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import "AppNetGlobalFeed.h"


@implementation AppNetGlobalFeed

+ (AppNetGlobalFeed *)sharedClient {
    static AppNetGlobalFeed * __client = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __client = [AppNetGlobalFeed new];
    });
    return __client;
}

- (void)requestMessages:(void (^)(AFHTTPRequestOperation *operation, id JSON))success
              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    AppNetApiClient *sharedClient = [AppNetApiClient sharedClient];
    
    [sharedClient getPath:@"global" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation, error);
    }];
}

@end
