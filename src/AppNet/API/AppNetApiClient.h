//
//  JSAPIClient.h
//  AppNet
//
//  Created by Joseph Southern on 10/3/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface AppNetApiClient : AFHTTPClient
+ (instancetype)sharedClient;
@end
