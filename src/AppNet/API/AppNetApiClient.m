//
//  JSAPIClient.m
//  AppNet
//
//  Created by Joseph Southern on 10/3/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import "AppNetApiClient.h"

static NSString * const AFAppDotNetAPIBaseURLString = @"https://alpha-api.app.net/stream/0/posts/stream/";

@implementation AppNetApiClient
+ (instancetype)sharedClient {
    static AppNetApiClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[AppNetApiClient alloc] initWithBaseURL:[NSURL URLWithString:AFAppDotNetAPIBaseURLString]];
    });
    
    return _sharedClient;
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [self setParameterEncoding:AFJSONParameterEncoding];
    [self setDefaultHeader:@"Content-Type" value:@"application/json"];
    [self setDefaultHeader:@"Accept" value:@"application/json"];
    
    return self;
}

@end
