//
//  AppNetGlobalFeed.h
//  AppNet
//
//  Created by Joseph Southern on 10/5/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppNetApiClient.h"

@interface AppNetGlobalFeed : NSObject
+ (AppNetGlobalFeed *)sharedClient;
- (void)requestMessages:(void (^)(AFHTTPRequestOperation *operation, id JSON))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
@end
