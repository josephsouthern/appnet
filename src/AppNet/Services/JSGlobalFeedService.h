//
//  JSAppNetService.h
//  AppNet
//
//  Created by Joseph Southern on 10/5/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppNetGlobalFeed.h"
#import "JSGlobalFeedDataSource.h"

@interface JSGlobalFeedService : NSObject
+ (JSGlobalFeedService *)sharedService;
- (void)requestMessages:(void(^)(id <JSGlobalFeedLogDataSource> dataSource))success failure:(void (^)(NSString *error))failure;
@end
