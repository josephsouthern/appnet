//
//  JSGlobalLogDataSource.h
//  AppNet
//
//  Created by Joseph Southern on 10/5/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JSGlobalFeedLogDataSource <NSObject>
- (NSArray*)sections;
- (NSArray*)sectionTitles;
@end
