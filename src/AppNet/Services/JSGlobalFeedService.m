//
//  JSAppNetService.m
//  AppNet
//
//  Created by Joseph Southern on 10/5/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import "JSGlobalFeedService.h"

@implementation JSGlobalFeedService
+ (JSGlobalFeedService *)sharedService {
    static JSGlobalFeedService *__sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedService = [JSGlobalFeedService new];
    });
    return __sharedService;
}

- (void)requestMessages:(void(^)(id <JSGlobalFeedLogDataSource> dataSource))success failure:(void (^)(NSString *error))failure {
    AppNetGlobalFeed *client = [AppNetGlobalFeed sharedClient];
    
    [client requestMessages:^(AFHTTPRequestOperation *operation, id JSON) {
        JSGlobalFeedDataSource *ds = [[JSGlobalFeedDataSource alloc] initWithJSON:JSON];
        success(ds);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(@"You had an error: %@");
    }];
}

@end
