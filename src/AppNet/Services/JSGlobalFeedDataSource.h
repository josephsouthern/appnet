//
//  JSAppNetMessage.h
//  AppNet
//
//  Created by Joseph Southern on 10/5/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSGlobalFeedLogDataSource.h"

@interface JSGlobalFeedDataSource : NSObject <JSGlobalFeedLogDataSource>
- (id)initWithJSON:(id)JSON;
@end