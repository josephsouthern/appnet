//
//  JSAppNetMessage.m
//  AppNet
//
//  Created by Joseph Southern on 10/5/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import "JSGlobalFeedDataSource.h"

@interface JSGlobalFeedDataSource ()
@property (nonatomic, strong) NSMutableArray *sectionTitles;
@property (nonatomic, strong) NSMutableArray *sections;
@property (nonatomic, strong) NSArray *jsonResponse;
@end

@implementation JSGlobalFeedDataSource
- (id)initWithJSON:(id)JSON {
    self = [super init];
    if (self) {
        self.jsonResponse = [JSON objectForKey:@"data"];
        self.sectionTitles = [NSMutableArray array];
        self.sections = [NSMutableArray array];
        [self.sectionTitles addObject:@"GlobalFeed"];
        [self buildSections];
        
    }
    return self;
}

- (void)buildSections {
    [self.sections addObject:self.jsonResponse];
}
@end
