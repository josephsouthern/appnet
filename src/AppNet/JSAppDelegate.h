//
//  JSAppDelegate.h
//  AppNet
//
//  Created by Joseph Southern on 10/3/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSMainTableViewController.h"

@interface JSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) JSMainTableViewController *viewController;
@property (nonatomic, strong) UINavigationController *navController;

@end
