//
//  JSMainTableViewController.m
//  AppNet
//
//  Created by Joseph Southern on 10/5/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import "JSMainTableViewController.h"

@interface JSMainTableViewController ()
@property (nonatomic, strong) id <JSGlobalFeedLogDataSource> dataSource;
@property (nonatomic, strong) JSMainTableCell *cellHelper;
@end

@implementation JSMainTableViewController

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"App.Net Global Feed", @"App.Net Global Feed");
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"App.Net Global Feed", @"App.Net Global Feed");
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"App.Net Global Feed", @"App.Net Global Feed");
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cellHelper = [[JSMainTableCell alloc] init];
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                [UIColor lightGrayColor], NSForegroundColorAttributeName,
                                                [UIFont fontWithName:@"Futura-Medium" size:21.0], NSFontAttributeName, nil]];
    self.title = @"App.Net Global Feed";
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh)
             forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self refresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)refresh {
    
    JSGlobalFeedService *svc = [JSGlobalFeedService sharedService];
    [svc requestMessages:^(id <JSGlobalFeedLogDataSource> dataSource) {
        self.dataSource = dataSource;
        [self.refreshControl endRefreshing];
        [self.tableView reloadData];
        
    } failure:^(NSString *error) {
        NSLog(@"That didn't work: %@", error);
        [self.refreshControl endRefreshing];
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.dataSource sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *currentSection = [[self.dataSource sections] objectAtIndex:section];
    return currentSection.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    //return [[self.dataSource sectionTitles] objectAtIndex:section];
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *currentSection = [[self.dataSource sections] objectAtIndex:indexPath.section];
    NSDictionary *item = [currentSection objectAtIndex:indexPath.row];

    return [self.cellHelper calculateCellHeight:[item objectForKey:@"text"]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    JSMainTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[JSMainTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
 
    // Configure the cell...
    NSArray *currentSection = [[self.dataSource sections] objectAtIndex:indexPath.section];
    NSDictionary *item = [currentSection objectAtIndex:indexPath.row];
    
    cell.nameLabel.text = [[item objectForKey:@"user"] objectForKey:@"name"];
    cell.messageLabel.text = [item objectForKey:@"text"];
    
    NSURL *url = [NSURL URLWithString:[[[item objectForKey:@"user"] objectForKey:@"avatar_image"] objectForKey:@"url"]];
    [cell.avatarImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@"avatar"]];
    cell.avatarImage.layer.cornerRadius = 9.0;
    cell.avatarImage.layer.masksToBounds = YES;
    
    [self adjustLabelHeight:cell.messageLabel];
    
    return cell;
}


- (void)adjustLabelHeight:(UILabel *)label{
    CGFloat width = 200;
    UIFont *font = label.font;
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          font, NSFontAttributeName,
                                          nil];
    
    CGRect frame = [label.text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                     attributes:attributesDictionary
                                                        context:nil];
    
    frame.origin = label.frame.origin;
    label.frame = frame;
}

@end
