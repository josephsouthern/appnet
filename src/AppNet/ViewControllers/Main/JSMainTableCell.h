//
//  JSMainTableCell.h
//  AppNet
//
//  Created by Joseph Southern on 10/5/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSMainTableCell : UITableViewCell
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *messageLabel;
@property (nonatomic, strong) UIImageView *avatarImage;

- (CGFloat)calculateCellHeight:(NSString*)messageText;

@end
