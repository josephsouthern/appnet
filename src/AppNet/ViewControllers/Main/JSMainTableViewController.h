//
//  JSMainTableViewController.h
//  AppNet
//
//  Created by Joseph Southern on 10/5/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSGlobalFeedService.h"
#import "JSMainTableCell.h"

@interface JSMainTableViewController : UITableViewController

@end
