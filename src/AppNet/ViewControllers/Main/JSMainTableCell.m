//
//  JSMainTableCell.m
//  AppNet
//
//  Created by Joseph Southern on 10/5/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import "JSMainTableCell.h"


@interface JSMainTableCell ()

@property (nonatomic) CGFloat DefaultMessageLabelWidth;
@property (nonatomic) CGFloat DefaultMessageLabelHeight;
@property (nonatomic, retain) UIFont *DefaultMessageSystemFont;
@property (nonatomic) CGFloat DefaultCellHeight;
@end

@implementation JSMainTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self.DefaultMessageSystemFont = [UIFont systemFontOfSize:14];
    self.DefaultMessageLabelWidth = 200.0f;
    self.DefaultMessageLabelHeight = 50.0f;
    self.DefaultCellHeight = 100.0f;
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.avatarImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 80, 80)];
        [self.contentView addSubview:self.avatarImage];
        
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(105, 14, 207, 24)];
        self.nameLabel.textAlignment = NSTextAlignmentLeft;
        self.nameLabel.font = [UIFont boldSystemFontOfSize:18];
        self.nameLabel.textColor = [UIColor darkTextColor];
        [self.contentView addSubview:self.nameLabel];
        
        self.messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(105, 38, 200, 50)];
        self.messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.messageLabel.numberOfLines = 0;
        self.messageLabel.textAlignment = NSTextAlignmentLeft;
        self.messageLabel.font = self.DefaultMessageSystemFont;
        
        [self.contentView addSubview:self.messageLabel];
    }
    return self;
}

- (CGFloat)calculateCellHeight:(NSString*)messageText{

    NSDictionary *attributesDictionary =
        [NSDictionary dictionaryWithObjectsAndKeys:self.DefaultMessageSystemFont, NSFontAttributeName, nil];
    
    CGRect frame = [messageText boundingRectWithSize:CGSizeMake(self.DefaultMessageLabelWidth, CGFLOAT_MAX)
                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                          attributes:attributesDictionary
                                                             context:nil];
    
    if (frame.size.height <= self.DefaultMessageLabelHeight)
        return self.DefaultCellHeight;
    else
        return self.DefaultCellHeight + (frame.size.height - self.DefaultMessageLabelHeight);

}


- (void)prepareForReuse {
    self.nameLabel.text = @"";
    self.messageLabel.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
}

@end