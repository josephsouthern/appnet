//
//  JSAppDelegate.m
//  AppNet
//
//  Created by Joseph Southern on 10/3/13.
//  Copyright (c) 2013 Joseph Southern. All rights reserved.
//

#import "JSAppDelegate.h"

@implementation JSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    JSMainTableViewController *vc = [[JSMainTableViewController alloc] initWithNibName:@"JSMainTableViewController" bundle:nil];
    self.navController = [[UINavigationController alloc] initWithRootViewController:vc];
    self.window.rootViewController = self.navController;
    [self.window makeKeyAndVisible];
    return YES;
}
@end
